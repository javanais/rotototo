# ROTOTOSCRIPT

# Test if the script works by running
# ./dragonframe_script.sh test test test POSITION 10
# Rotototo should move frame to 10

echo "Test" >> /Volumes/javanai/zDev/git/rotototo/dragonscripts/dragonframe_script_log.txt
if [ "$4" == "POSITION" ]
then
echo "Move ROTOTO to frame $5" >> /Volumes/javanai/zDev/git/rotototo/dragonscripts/dragonframe_script_log.txt
/Volumes/javanai/zDev/git/rotototo/dragonscripts/dragon_pilot.rb $5
fi

# uncomment these lines to create logs
# echo "Take       : $3" >> ./dragonframe_script_log.txt
# echo "Action     : $4" >> ./dragonframe_script_log.txt
# echo "Frame      : $5" >> ./dragonframe_script_log.txt


# INSTALLATION
# put dragonframe_script.sh and dragon_pilot.rb in the same folder
# run 'chmod u+x dragonframe_script.sh' to make it executable.
# run 'chmod u+x dragon_pilot.rb' to make it executable.
#
# In Dragonframe, go to the Advanced tab of Preferences
# and choose this file for Action Script.

# TROUBLE SHOOTING
# run "./dragon_pilot.rb 5"
# Look at the error, try to understand what it concerns.

# If you are really really stuck, copy the error and contact john@lamenagerie.com
# He may reply
# Offer him  a beer or some money and he probably will






# Other commands for dragon

# "POSITION" is sent whenever Dragonframe moves to a new frame.
#   This is probably what you want to use. After shooting
#   frame 10, when Dragonframe is ready to capture frame 11, it
#   sends "POSITION 11".
# "SHOOT" happens immediately before shooting.
# "DELETE" happens immediately before deleting.
# "CC" is capture complete. There is an additional argument,
#   the file name of the main downloaded image.
# "FC" is frame complete. There is an additional argument,
#   the file name of the main downloaded image.
# "TEST" is test shot complete. There is an additional argument,
#   the file name of the main downloaded image.
# "EDIT" means a timeline edit (cut/copy/paste frames) is
#   complete.
# "CONFORM" means a conform edits is complete.



#echo "Production : $1" >> ~/dragonframe_script_log.txt
#echo "Scene      : $2" >> ~/dragonframe_script_log.txt

#echo "Exposure   : $6" >> /dragonframe_script_log.txt
#echo "Exp. Name  : $7" >> ~/dragonframe_script_log.txt
#echo "Filename   : $8" >> ~/dragonframe_script_log.txt


if [ "$4" == "CC" ]
then

echo "CC $5-$6 -> $8" >> ./dragonframe_script_log.txt

fi

if [ "$4" == "FC" ]
then

echo "FC $5-$6" >> ./dragonframe_script_log.txt

fi

if [ "$4" == "EDIT" ]
then

echo "EDIT $5-$6" >> ./dragonframe_script_log.txt

fi