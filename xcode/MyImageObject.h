//
//  MyImageObject.h
//  RotoToto
//
//  Created by javanai on 06/11/14.
#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
@interface MyImageObject : NSObject
{
    NSURL *url;
}

@property (retain) NSURL *url;

@end