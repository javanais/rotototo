/*
     File: Controller.m 
 Abstract: The primary window controller of this application. 
  Version: 1.1 
  
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple 
 Inc. ("Apple") in consideration of your agreement to the following 
 terms, and your use, installation, modification or redistribution of 
 this Apple software constitutes acceptance of these terms.  If you do 
 not agree with these terms, please do not use, install, modify or 
 redistribute this Apple software. 
  
 In consideration of your agreement to abide by the following terms, and 
 subject to these terms, Apple grants you a personal, non-exclusive 
 license, under Apple's copyrights in this original Apple software (the 
 "Apple Software"), to use, reproduce, modify and redistribute the Apple 
 Software, with or without modifications, in source and/or binary forms; 
 provided that if you redistribute the Apple Software in its entirety and 
 without modifications, you must retain this notice and the following 
 text and disclaimers in all such redistributions of the Apple Software. 
 Neither the name, trademarks, service marks or logos of Apple Inc. may 
 be used to endorse or promote products derived from the Apple Software 
 without specific prior written permission from Apple.  Except as 
 expressly stated in this notice, no other rights or licenses, express or 
 implied, are granted by Apple herein, including but not limited to any 
 patent rights that may be infringed by your derivative works or by other 
 works in which the Apple Software may be incorporated. 
  
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE 
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION 
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS 
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND 
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS. 
  
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL 
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, 
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED 
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), 
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE 
 POSSIBILITY OF SUCH DAMAGE. 
  
 Copyright (C) 2012 Apple Inc. All Rights Reserved. 
  
 */

#import "Controller.h"
#import "EchoServer.h"
#import "MyImageObject.h"
//#import "ControllerBrowsing.h"





@implementation Controller
@synthesize selectedImagePath;
@synthesize offsetValueTextfield=_offsetValueTextfield;

- (void)updateScreenOptions:(NSScreen *)screen
{
 	if (screen != nil)
	{
        NSMutableDictionary *screenOptions =
        [[[NSWorkspace sharedWorkspace] desktopImageOptionsForScreen:curScreen] mutableCopy];
		
		// the value is an NSNumber containing an NSImageScaling (scaling factor)
		NSNumber *scalingFactor = [screenOptions objectForKey:NSWorkspaceDesktopImageScalingKey];
        
        // select the proper menu item that matches it's representedObject value
        for (NSMenuItem *item in [scalingPopup itemArray])
        {
            if ([[item representedObject] integerValue] == [scalingFactor integerValue])
            {
                [scalingPopup selectItem:item];
                break;
            }
        }
		
		// the value is an NSNumber containing a BOOL (allow clipping)
		//NSNumber *allowClipping = [screenOptions objectForKey:NSWorkspaceDesktopImageAllowClippingKey];
		//[[clippingCheckbox cell] setState:[allowClipping boolValue]];
		
		//// the value is an NSColor (fill color)
		//NSColor *fillColorValue = [screenOptions objectForKey:NSWorkspaceDesktopImageFillColorKey];
		//if (fillColorValue)
		//	[fillColor setColor:fillColorValue];
        
        // replace out the old scaling factor with the new
       [screenOptions setObject: [NSNumber numberWithInt:NSImageScaleProportionallyUpOrDown] forKey:NSWorkspaceDesktopImageScalingKey];
        [screenOptions setObject:[NSColor blackColor] forKey:NSWorkspaceDesktopImageFillColorKey];
        NSError *error = nil;
        NSURL *imageURL = [[NSWorkspace sharedWorkspace] desktopImageURLForScreen:curScreen];
        if (![[NSWorkspace sharedWorkspace] setDesktopImageURL:imageURL
                                                     forScreen:curScreen
                                                       options:screenOptions
                                                         error:&error])
        {
            [self presentError:error];
        }
        
        [screenOptions release];
	}
}

static NSArray* openFiles()
{
	// Get a list of extensions to filter in our NSOpenPanel.
	NSOpenPanel* panel = [NSOpenPanel openPanel];
    
    [panel setCanChooseDirectories:YES];	// The user can choose a folder; images in the folder are added recursively.
    [panel setCanChooseFiles:YES];
	[panel setAllowsMultipleSelection:YES];
    
    [panel setAllowedFileTypes:[NSImage imageUnfilteredTypes]];
    NSInteger result = [panel runModal];
    if (result == NSFileHandlingPanelOKButton)
		return [panel URLs];
    
    return nil;
} 


- (void)awakeFromNib
{

    /*
    [self.window setStyleMask:NSBorderlessWindowMask];
    
    //set 'z-index' of window to display on top of menubar
    [self.window setLevel:NSMainMenuWindowLevel+1];
    
    //set the window rect to the screen rect
    NSRect mainDisplayRect = [[NSScreen mainScreen] frame];
    [self.window setFrame:mainDisplayRect display:YES];

   
    */
    self.stepperObject.delegate=self;
        dragonIndex=3;
    [self.frameNumberTextField setSelectable:NO];
    [imageBrowser setZoomValue:0.2f];
    //start server
        self.server = [[EchoServer alloc] init];
    self.server.delegate=self;
        if ( [self.server start] ) {
            NSLog(@"Started server on port %zu.", (size_t) [self.server port]);
           
        } else {
            NSLog(@"Error starting server");
        }
    
    
	//[self setupBrowsing];	// setup our image browser and initially point it to: /Library/Desktop Pictures/
	
	// build the screens popup menu
	NSMenu *screensMenu = [[NSMenu alloc] initWithTitle:@"screens"];
	NSArray *screens = [NSScreen screens];
	if([screens count]<2){
        [errorMessage setStringValue:@"Hey patate, Branche le video projector"];
        return;
    }
    
	curScreen = [screens objectAtIndex:1];
	[screenPopup setMenu:screensMenu];
	[screensMenu release];
	
	[self updateScreenOptions:curScreen];
}

-(void) stepperValueChanged:(int)stepperValue {
    [NSTimer scheduledTimerWithTimeInterval:.1f target:self selector:@selector(updateInterface) userInfo:nil repeats:NO];
}

-(void)updateInterface {
    int newIndex=self.stepperObject.stepperValue + dragonIndex;
    NSMutableIndexSet *myImmutableIndexes=[[NSMutableIndexSet alloc] init];
    [myImmutableIndexes addIndex: newIndex];
    [imageBrowser setSelectionIndexes: myImmutableIndexes byExtendingSelection:NO];
}

- (void)dealloc
{
	[images release];
	[super dealloc];
}

- (IBAction)screensMenuAction:(id)sender
{
	NSMenuItem *chosenItem = (NSMenuItem *)sender;
	NSScreen *screen = [chosenItem representedObject];
	curScreen = screen;	// keep track of the current screen selection
	[self updateScreenOptions:screen];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}


#pragma mark - Actions

- (IBAction)scalingAction:(id)sender {
	// get the current screen options
	NSMutableDictionary *screenOptions =
        [[[NSWorkspace sharedWorkspace] desktopImageOptionsForScreen:curScreen] mutableCopy];
	
	// the value is the representedObject of the menu item describing the NSNumber
    // containing an NSImageScaling (scaling factor)
	NSPopUpButton *popupButton = sender;
    NSNumber *scalingFactor = [[popupButton selectedItem] representedObject];
    
	// replace out the old scaling factor with the new
	[screenOptions setObject:scalingFactor forKey:NSWorkspaceDesktopImageScalingKey];
	
	NSError *error = nil;
	NSURL *imageURL = [[NSWorkspace sharedWorkspace] desktopImageURLForScreen:curScreen];
	if (![[NSWorkspace sharedWorkspace] setDesktopImageURL:imageURL
                                                 forScreen:curScreen
                                                   options:screenOptions
                                                     error:&error]) {
		[self presentError:error];
	}
	
	[screenOptions release];
}

- (IBAction)allowClippingAction:(id)sender {
    NSButton *checkbox = sender;
	NSNumber *allowClipping = [NSNumber numberWithBool:[checkbox state]];
    NSLog(@"allowClipping %d", [allowClipping intValue]);
    NSURL *url;
    NSDictionary *screenOptions   = [[NSWorkspace sharedWorkspace] desktopImageOptionsForScreen:curScreen];;
	// get the current screen options
    if([allowClipping intValue]==0){
        NSString *fileName = [[NSBundle mainBundle] pathForResource:@"blacksquare" ofType:@"jpg"];
        url = [NSURL URLWithString: [NSString stringWithFormat:@"file://%@",  fileName]];
    }else {
        url=self.selectedImagePath;
    }
        
    NSNumber *isDirectoryFlag = nil;
    if ([url getResourceValue:&isDirectoryFlag forKey:NSURLIsDirectoryKey error:nil] && ![isDirectoryFlag boolValue]) {
        NSError *error = nil;
        [[NSWorkspace sharedWorkspace] setDesktopImageURL:url
                                                forScreen:curScreen
                                                  options:screenOptions
                                                    error:&error];
        if (error) {
            [NSApp presentError:error];
        }
    }
}

- (IBAction) fillColorAction:(id) sender {
	// get the current screen options
	NSMutableDictionary *screenOptions =
        [[[NSWorkspace sharedWorkspace] desktopImageOptionsForScreen:curScreen] mutableCopy];

	// the value is an NSColor (fill color)
	NSColorWell *colorWell = sender;
	NSColor *fillColorValue = [colorWell color];

	// replace out the old fill color with the new
	[screenOptions setObject:fillColorValue forKey:NSWorkspaceDesktopImageFillColorKey];
	
	NSError *error = nil;
	NSURL *imageURL = [[NSWorkspace sharedWorkspace] desktopImageURLForScreen:curScreen];
	if (![[NSWorkspace sharedWorkspace] setDesktopImageURL:imageURL
                                                 forScreen:curScreen
                                                   options:screenOptions
                                                     error:&error]) {
		[self presentError:error];
	}
	
	[screenOptions release];
}

#pragma mark - server delegate
- (void) dragonMessageReceived:(NSString*) message{

  //  NSLog(@"delegated msg %i", [message intValue]);
    NSMutableIndexSet *myImmutableIndexes=[[NSMutableIndexSet alloc] init];
    [myImmutableIndexes addIndex: [message intValue]+ self.stepperObject.stepperValue];
    dragonIndex=[message intValue];
    [imageBrowser setSelectionIndexes: myImmutableIndexes byExtendingSelection:NO];
}

- (IBAction) selectImageFolderButtonClicked:(NSButton *)sender {
    NSArray* path = openFiles();
    if (path) {
        // launch import in an independent thread
		[NSThread detachNewThreadSelector:@selector(addImagesWithPaths:) toTarget:self withObject:path];
	}
}

- (void)addImagesWithPaths:(NSArray*)paths {
    if(images){
        [images release];
    }
    images = [[NSMutableArray alloc] init];
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    [paths retain];
    
    NSInteger i, n;
	n = [paths count];
    for (i = 0; i < n; i++) {
        NSURL *url = [paths objectAtIndex:i];
		[self addImagesWithPath:[url path]];
    }
    [imageBrowser reloadData];
	// Update the data source in the main thread.
    // [self performSelectorOnMainThread:@selector(updateDatasource) withObject:nil waitUntilDone:YES];
    [paths release];
    [pool release];
}

- (void)addImagesWithPath:(NSString*)path {
    BOOL dir;
    [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&dir];
    
    if (dir) {
		NSInteger i, n;
		NSArray* content = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
        n = [content count];
        
		// Parse the directory content.
        for ( i = 0; i < n; i++ ) {
			[self addAnImageWithPath:[path stringByAppendingPathComponent:[content objectAtIndex:i]]];
        }
    } else {
		[self addAnImageWithPath:path];
	}
}

- (void)addAnImageWithPath:(NSString*)path {
	BOOL addObject = NO;

	NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
	if (fileAttribs) {
		if ([NSFileTypeDirectory isEqualTo:[fileAttribs objectForKey:NSFileType]]){
			if ([[NSWorkspace sharedWorkspace] isFilePackageAtPath:path] == NO)
				addObject = YES;
		} else {
			addObject = YES;
		}
	}
	
	if (addObject && [self isImageFile:path]) {
        MyImageObject * item = [[MyImageObject alloc] init];
        item.url = [[[NSURL alloc] initFileURLWithPath:path] autorelease];
        [images addObject:item];

        [item release];
	}
}


- (BOOL)isImageFile:(NSString*)filePath {
	BOOL				isImageFile = NO;
	LSItemInfoRecord	info;
	CFStringRef			uti = NULL;
	
	CFURLRef url = CFURLCreateWithFileSystemPath(NULL, (CFStringRef)filePath, kCFURLPOSIXPathStyle, FALSE);
	
	if (LSCopyItemInfoForURL(url, kLSRequestExtension | kLSRequestTypeCreator, &info) == noErr) {
		// Obtain the UTI using the file information.
		
		// If there is a file extension, get the UTI.
		if (info.extension != NULL) {
			uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, info.extension, kUTTypeData);
			CFRelease(info.extension);
		}
        
		// No UTI yet
		if (uti == NULL) {
			// If there is an OSType, get the UTI.
			CFStringRef typeString = UTCreateStringForOSType(info.filetype);
			if ( typeString != NULL) {
				uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassOSType, typeString, kUTTypeData);
				CFRelease(typeString);
			}
		}
		
		// Verify that this is a file that the ImageIO framework supports.
		if (uti != NULL) {
			CFArrayRef  supportedTypes = CGImageSourceCopyTypeIdentifiers();
			CFIndex		i, typeCount = CFArrayGetCount(supportedTypes);
            
			for (i = 0; i < typeCount; i++) {
				if (UTTypeConformsTo(uti, (CFStringRef)CFArrayGetValueAtIndex(supportedTypes, i))) {
					isImageFile = YES;
					break;
				}
			}
            
            CFRelease(supportedTypes);
            CFRelease(uti);
		}
	}
    
    CFRelease(url);
	return isImageFile;
}

@end
